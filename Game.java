import java.util.Stack;

/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author Robin de Koster
 * @version 20-11-2017
 */

public class Game 
{
    private Parser parser;
    private Player player;
    private int time;
    
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        player = new Player();
        parser = new Parser();
        time = 20;
        createRooms();
    }
    
    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        //create the rooms and streets
        Room lawn, pastorijstraat, grandmahouse, gh_cellar, gh_attic, hogehuisstraat, mansion, winevault;
        Room church, toilets, toiletstall, pulpit, visserstraat, n451a, n451b, n451c;
        Room highway, engelsesteenweg, liefkenshoekstraat, dock, teleport_room, trapdoor;

        // create the rooms
        lawn            = new Room("on the lawn with creepy totem poles.");
        grandmahouse    = new Room("inside the only normal looking house here, but it smells like grandma.");
        gh_cellar       = new Room("??? too dark!");
        gh_attic        = new Room("inside the (too) tight arric.");
        mansion         = new Room("inside a big, expensive looking mension. Everything of value appears to be stolen.");
        winevault       = new Room("in the Winevault, surrounded by expensive, old wine here.");
        church          = new Room("inside the HemelVaart Church, it's very cold in here.");
        toilets         = new Room("inside the -still stinking- toilets of this church.");
        toiletstall     = new Room("inside a toiletstall, why did you want this again?.");
        pulpit          = new Room("looking at the Pulpit, this is were the priest sat.");
        highway         = new Room("at the highway, no cars here, that is wierd!");
        dock            = new Room("at the dock and can see all over the sea.");
        teleport_room   = new Room("in the teleportation room! you can only go back to the cellar from grandma house. :sadpepe:");
        trapdoor        = new Room("You fell through a trapdoor!");
        
        // creating streets
        liefkenshoekstraat  = new Room("walking through the Liefkenshoekstraat, you feel tired.");
        engelsesteenweg     = new Room("walking through the Engelsesteenweg. I think this is the exit! Escape!");
        hogehuisstraat      = new Room("walking through the Hogehuisstraat, no houses here.");
        pastorijstraat      = new Room("walking through the Pastorijstraat, nothing here.");
        visserstraat        = new Room("walking through the Visserstraat, looks like a dead-end.");
        n451a               = new Room("walking through the N451A, nothing special.");
        n451b               = new Room("walking through the even worse lighted street, N451B.");
        n451c               = new Room("walking through the badly lighted N451C street.");

        // create the items and place them in rooms
        lawn                    .addItem(new Item("bacon", 50));
        lawn                    .addItem(new Item("torch", 1000));
        grandmahouse            .addItem(new Item("knitting_needles", 100));
        grandmahouse            .addItem(new Item("cat_food", 1000));
        gh_cellar               .addItem(new Item("hair_ball", 10));
        liefkenshoekstraat      .addItem(new Item("slippers", 300));
        pulpit                  .addItem(new Item("Bible", 200));
        toiletstall             .addItem(new Item("paper", 20));
        n451a                   .addItem(new Item("rotted_meat", 500));
        dock                    .addItem(new Item("safety_helmet", 500));
        mansion                 .addItem(new Item("sword", 4500));
        winevault               .addItem(new Item("wine", 2000));
        winevault               .addItem(new Item("corkscrew", 250));
        n451c                   .addItem(new Item("lightsaber", 1000));
        highway                 .addItem(new Item("tire", 2000));
        visserstraat            .addItem(new Item("fishingrod", 500));
        visserstraat            .addItem(new Item("golden_key", 2000));
        n451b                   .addItem(new Item("cookie", 50));
        

        // initialise room exits
        lawn.setExit("east", n451a);
        lawn.setExit("south", n451b);
        lawn.setExit("west", pastorijstraat);
        
        pastorijstraat.setExit("north", grandmahouse);
        pastorijstraat.setExit("east", lawn);
        pastorijstraat.setExit("west", n451c);
        
        grandmahouse.setExit("south", pastorijstraat);
        grandmahouse.setExit("down", gh_cellar);
        grandmahouse.setExit("up", gh_attic);
        gh_cellar.setExit("up", grandmahouse);
        gh_attic.setExit("down", grandmahouse);
        gh_attic.setExit("up", trapdoor);
        trapdoor.setExit("down", gh_cellar);
        
        hogehuisstraat.setExit("north", n451c);
        
        mansion.setExit("east", n451c);
        mansion.setExit("down", winevault);
        winevault.setExit("up", mansion);
        
        church.setExit("right_corner", toilets);
        church.setExit("left_corner", pulpit);
        church.setExit("east", n451b);
        church.setExit("west", n451c);
        toilets.setExit("back", church);
        toilets.setExit("door", toiletstall);
        toiletstall.setExit("back", toilets);
        pulpit.setExit("back", church);
        
        visserstraat.setExit("north", n451b);
        
        highway.setExit("south", n451a);
        highway.setExit("north", teleport_room);
        highway.setExit("west", engelsesteenweg);
        
        engelsesteenweg.setExit("None, no escaping Doel in this game.", engelsesteenweg);
        
        liefkenshoekstraat.setExit("north", n451a);
        
        dock.setExit("west", n451a);
        
        teleport_room.setExit("there", gh_cellar);
        
        n451a.setExit("north", highway);
        n451a.setExit("east", dock);
        n451a.setExit("south", liefkenshoekstraat);
        n451a.setExit("west", lawn);
        n451a.setExit("south_west", n451b);
        
        n451b.setExit("north", lawn);
        n451b.setExit("east", n451a);
        n451b.setExit("south", visserstraat);
        n451b.setExit("west", church);
        
        n451c.setExit("east", pastorijstraat);
        n451c.setExit("south_east", church);
        n451c.setExit("south", hogehuisstraat);
        n451c.setExit("west", mansion);
        
        // setup start room
        player.setCurrentRoom(lawn);
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();
        player.printPlayerName();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.print('\u000C');
        System.out.println();
        System.out.println("    >   Doel, The Adventurous Adventure!   <");
        System.out.println();
        System.out.println("------------------Introduction------------------");
        System.out.println("Welcome to Doel, Oost-Vlaanderen, Belgium.");
        System.out.println("Doel is the ghost town of Belgium.");
        System.out.println("Type 'help' to get help playing the game.");
        System.out.println("------------------------------------------------");
        System.out.println();
        player.printLocationInfo();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("Invalid Command! ");
            availableCommands();
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go")) {
            player.goRoom(command);
            moveChecker();
        }
        else if (commandWord.equals("look")) {
            player.look();
        }
        else if (commandWord.equals("listen")) {
            player.listen();
        }
        else if (commandWord.equals("eat")) {
            player.eat(command);
        }
        else if (commandWord.equals("get")) {
            player.pickUpItem(command);
        }
        else if (commandWord.equals("drop")) {
            player.dropItem(command);
        }
        else if (commandWord.equals("items")) {
            player.listItems();
        }
        else if (commandWord.equals("back")) {
            player.back(command);
        }
        else if (commandWord.equals("change_name")) {
            player.setPlayerName(command);
        }
        else if (commandWord.equals("weight")) {
            player.getWeight();
        }
        else if (commandWord.equals("name")) {
            player.printPlayerName();
        }
        else if (commandWord.equals("quit")) {
            wantToQuit = player.quit(command);
        }

        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Checks if the players movements is within
     * borders.
     */
    private void moveChecker()
    {
        time -= 1;
        if(time == 0) {
            System.out.print("You moved too many times " + player.getPlayerName() + ", game over!");
            System.exit(1);
        }
    }
   
    /**
     * Print out valid commands.
     */
    private void availableCommands()
    {
        System.out.println();
        System.out.println("--------------------------");
        System.out.print(player.getPlayerName() + ", your valid Command Words Are:\n");
        String[] validCommands = parser.showCommands();
        for (int i = 0; i < validCommands.length; i++)
        {
            System.out.print("> " + validCommands[i].toUpperCase() + "\n");
        }
        System.out.println("--------------------------");
        System.out.println();
    }

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println('\u000C');
        System.out.println("You are lost. You are alone. You wander");
        System.out.println("around the ghost town of Belgium, Doel.");
        // availableCommands();         // unused because i want a description of the command.
        System.out.println();
        System.out.println("To win the game: Escape Doel.");
        System.out.println();
        System.out.println("--------------------COMMANDS--------------------");
        System.out.println("Type 'go' + direction to move.");
        System.out.println("Type 'look' to look around the room.");
        System.out.println("Type 'help' if you need help.");
        System.out.println("Type 'listen' to listen if there's anyone around.");
        System.out.println("Type 'get' to take an item from the room.");
        System.out.println("Type 'drop' to drop your item(s) in the room");
        System.out.println("Type 'items' to see your inventory.");
        System.out.println("Type 'back' to go back to your previous room.");
        System.out.println("Type 'change_name' to change your name.");
        System.out.println("Type 'name' to get your name, if you forgot.");
        System.out.println("Type 'quit' if you want to quit the game.");
        System.out.println("------------------------------------------------");
        player.listItems();
        System.out.println();
        player.printLocationInfo();
    }
}
